package Base;

import io.restassured.response.Response;

import java.util.ArrayList;
import java.util.List;

public class Create5Users extends BASE {


    public List<Response> Createting5Users() {
        List<Response> lists = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Object obj = data.get_user_data();
            Response res = POST(url + users_path, obj, token);
            lists.add(res);
        }
        return lists;
    }

    public List<Integer> GetList4ID() {
        List<Integer> list4 = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (i != random_position) {
                list4.add(list.get(i).jsonPath().getInt("data.id"));
            }
        }
        return list4;
    }

    public List<Integer> GetList5ID() {
        List<Integer> list5 = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            list5.add(list.get(i).jsonPath().getInt("data.id"));
        }
        return list5;
    }
}
