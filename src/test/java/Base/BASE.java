package Base;

import com.google.gson.Gson;
import data.UsersData;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import models.tokens;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

import static io.restassured.RestAssured.given;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

public class BASE {
    public String url = "https://gorest.co.in";
    public String users_path = "/public-api/users/";
    public String token = "980482251c0446b8b4ef4b7801fea1aa933dad0337e56c91fec68ab2ddf8a27c";

    //in4
    public String gender = "Male";
    public String status = "Active";
    LocalDateTime now = LocalDateTime.now();
    public int seconds = now.getSecond();

    //list users
    public static UsersData data = new UsersData();
    public static Random random = new Random();
    public static Create5Users tool = new Create5Users();
    public static List<Response> list = tool.Createting5Users();

    public static List<Integer> list_5_users_id = tool.GetList5ID();
    public static int random_position = random.nextInt(list.size());
    public static int random_id = list.get(random_position).jsonPath().getInt("data.id");

    public static List<Integer> list_4_users_id = tool.GetList4ID();
    public static int random_position_in_4 = random.nextInt(list_4_users_id.size());
    public static int random_id_in_4 = list_4_users_id.get(random_position_in_4);


    public String randomEmail(){
        String email = randomAlphanumeric(10)+"@gmail.com";
        return email;
    }
    public String randomName(){
        String name = "Mr."+randomAlphanumeric(5);
        return name;
    }


    public Response GET(String url){
        Response res = given()
                .contentType(ContentType.JSON)
                .when()
                .get(url);
        return res;
    }

    public Response POST(String url, Object obj, String token){
        Response res = RestAssured.given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .when()
                .body(obj)
                .post(url);
        return res;
    }
    public void PUT(String url, Object obj, String token){
        Response res = RestAssured.given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .when()
                .body(obj)
                .put(url);

    }
    public Response PATCH(String url, Object obj, String token){
        Response res = RestAssured.given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .when()
                .body(obj)
                .patch(url);
        return res;
    }

    public Response DELETE(String url, String token){
        Response res = RestAssured.given()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .when()
                .delete(url);
        return res;
    }

    public String getToken(){
        tokens tokens = new tokens();
        tokens.setToken(token);
        String token = tokens.getToken();
        return token;
    }

    public String convertJsonToString(Object obj){
        Gson gson = new Gson();
        return gson.toJson(obj);
    }

}
