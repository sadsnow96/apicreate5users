package data;

import Base.BASE;
import models.users;

public class UsersData extends BASE {
    public Object get_user_data(){
        users user = new users();
        user.setName(randomName());
        user.setGender(gender);
        user.setEmail(randomEmail());
        user.setStatus(status);
        return user;
    }
}
