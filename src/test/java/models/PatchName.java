package models;

public class PatchName {
    private String name;

    public PatchName() {
    }

    public PatchName(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
