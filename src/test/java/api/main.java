package api;

import Base.BASE;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import Base.PatchRandomID;
import Base.Delete1User;

public class main extends BASE {

    @Test(priority = 0)
    public void STEPcreate5Users(){
        System.out.println("5 users below:============================================>");
        for (int i=0;i<list.size();i++){
            list.get(i).prettyPrint();
        }
        System.out.println("list 5 id: "+list_5_users_id);
        System.out.println("created 5 users have done=================================>");
    }

    @Test(priority = 1)
    public void STEPpatchRandomUsers(){
        PatchRandomID patch_tool = new PatchRandomID();
        Response res = patch_tool.patchRandomUser();
        System.out.print("patch id: ");
        System.out.println(res.jsonPath().getInt("data.id")+"===============================>");
        res.prettyPrint();
        System.out.println("patched have done=========================================>");
    }
    @Test(priority = 2)
    public void STEPdeleteRandomUsers(){
        Delete1User delete_tool = new Delete1User();
        System.out.println("list 4 id: "+list_4_users_id);
        System.out.println("delete id: "+random_id_in_4);
        Response res = delete_tool.deleteRandomUser();
        System.out.println("status code: "+res.getStatusCode());
        System.out.println("deleted============================================>");
    }
}
